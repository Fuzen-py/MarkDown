extern crate markdown_parser;
use markdown_parser::{ListType, MarkDown};

fn get_list(s: &str) -> Vec<ListType> {
    let mut results = markdown_parser::parse(s).unwrap();
    assert_eq!(results.len(), 1);
    match results.remove(0) {
        MarkDown::List(list) => list,
        err => panic!("Expected list, found {:?}", err),
    }
}

#[test]
fn bullet_list() {
    let languages_list = "* Rust\n* Python\n* Haskell\n";
    let list = get_list(languages_list);
    assert_eq!(list.len(), 3);
    list.into_iter().enumerate().for_each(|(i, e)| match e {
        ListType::Bullet(txt) => match i {
            0 => assert_eq!(txt, "Rust"),
            1 => assert_eq!(txt, "Python"),
            2 => assert_eq!(txt, "Haskell"),
            _ => unreachable!(),
        },
        err => panic!("Expected Bullet list, found {:?}", err),
    });
}

#[test]
fn unord_list() {
    let languages_list = "- Rust\n- Python\n- Haskell";
    let list = get_list(languages_list);
    assert_eq!(list.len(), 3);
    list.into_iter().enumerate().for_each(|(i, e)| match e {
        ListType::UnOrd(txt) => match i {
            0 => assert_eq!(txt, "Rust"),
            1 => assert_eq!(txt, "Python"),
            2 => assert_eq!(txt, "Haskell"),
            _ => unreachable!(),
        },
        err => panic!("Expected Unord list, found {:?}", err),
    });
}

#[test]
fn test_numbered() {
    let languages_list = "1. Rust\n2. Python\n3. Haskell";
    let list = get_list(languages_list);
    assert_eq!(list.len(), 3);
    list.into_iter().enumerate().for_each(|(i, e)| match e {
        ListType::Numbered(num, txt) => match i {
            0 => {
                assert_eq!(txt, "Rust");
                assert_eq!(num, 1);
            }
            1 => {
                assert_eq!(txt, "Python");
                assert_eq!(num, 2)
            }
            2 => {
                assert_eq!(txt, "Haskell");
                assert_eq!(num, 3);
            }
            _ => unreachable!(),
        },
        err => panic!("Expected Unord list, found {:?}", err),
    });
}

#[test]
fn test_nested_unord() {
    let languages_list =
        "- Rust\n    - Rocket\n    - Diesel\n- Python\n    - Flask\n    - Sqlalchemy";
    let list = get_list(languages_list);
    assert_eq!(list.len(), 6);
    list.into_iter().enumerate().for_each(|(i, e)| match i {
        0 => match e {
            ListType::UnOrd(txt) => assert_eq!(txt, "Rust"),
            err => panic!("Expected UnOrd(\"Rust\") found {:?}", err),
        },
        1 => match e {
            ListType::Nested(depth, list) => {
                assert_eq!(depth, 4);
                match *list {
                    ListType::UnOrd(txt) => assert_eq!(txt, "Rocket"),
                    err => panic!("Expected UnOrd(\"Rocket\"), found {:?}", err),
                }
            }
            err => panic!("Expected Nested(4, UnOrd(\"Rocket\")), found {:?}", err),
        },
        2 => match e {
            ListType::Nested(depth, list) => {
                assert_eq!(depth, 4);
                match *list {
                    ListType::UnOrd(txt) => assert_eq!(txt, "Diesel"),
                    err => panic!("Expected UnOrd(\"Diesel\"), found {:?}", err),
                }
            }
            err => panic!("Expected Nested(4, UnOrd(\"Diesel\")), found {:?}", err),
        },
        3 => match e {
            ListType::UnOrd(txt) => assert_eq!(txt, "Python"),
            err => panic!("Expected UnOrd(\"Python\") found {:?}", err),
        },
        4 => match e {
            ListType::Nested(depth, list) => {
                assert_eq!(depth, 4);
                match *list {
                    ListType::UnOrd(txt) => assert_eq!(txt, "Flask"),
                    err => panic!("Expected UnOrd(\"Flask\"), found {:?}", err),
                }
            }
            err => panic!("Expected Nested(4, UnOrd(\"Flask\")), found {:?}", err),
        },
        5 => match e {
            ListType::Nested(depth, list) => {
                assert_eq!(depth, 4);
                match *list {
                    ListType::UnOrd(txt) => assert_eq!(txt, "Sqlalchemy"),
                    err => panic!("Expected UnOrd(\"Sqlalchemy\"), found {:?}", err),
                }
            }
            err => panic!("Expected Nested(4, UnOrd(\"Sqlalchemy\")), found {:?}", err),
        },
        _ => (),
    })
}

#[test]
fn custom_list() {
    let languages_list = "-{code} Rust\n-{code} Python\n-{code} Haskell";
    let list = get_list(languages_list);
    assert_eq!(list.len(), 3);
    list.into_iter().enumerate().for_each(|(i, e)| match e {
        ListType::Custom(custom, txt) => match i {
            0 => {
                assert_eq!(txt, "Rust");
                assert_eq!(custom, "code")
            }
            1 => assert_eq!(txt, "Python"),
            2 => assert_eq!(txt, "Haskell"),
            _ => unreachable!(),
        },
        err => panic!("Expected Bullet list, found {:?}", err),
    });
}
