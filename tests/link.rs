extern crate markdown_parser;
use markdown_parser::LinkType;

fn get_link(s: &str) -> LinkType {
    match s.parse() {
        Ok(link) => link,
        Err(why) => panic!("{:?}", why),
    }
}

#[test]
fn absolute_named_link() {
    let txt = "[MacDown logo](http://macdown.uranusjr.com/static/images/logo-160.png)";
    match get_link(txt) {
        LinkType::NamedAbsolute(name, url) => {
            assert_eq!(name, "MacDown logo");
            assert_eq!(
                url,
                "http://macdown.uranusjr.com/static/images/logo-160.png"
            );
        }
        link => panic!("Expected NamedAbsolute link, found {:?}", link),
    }
}

#[test]
fn absolute_link() {
    let txt = "http://macdown.uranusjr.com/static/images/logo-160.png";
    match get_link(txt) {
        LinkType::Absolute(url) => {
            assert_eq!(url, txt);
        }
        link => panic!("Expected Absolute link, found {:?}", link),
    }
}

#[test]
fn named_relative_link() {
    let txt = "[MacDown logo](../static/images/logo-160.png)";
    match get_link(txt) {
        LinkType::NamedRelative(name, url) => {
            assert_eq!(url, "../static/images/logo-160.png");
            assert_eq!(name, "MacDown logo")
        }
        link => panic!("Expected NamedRelative link, found {:?}", link),
    }
}
