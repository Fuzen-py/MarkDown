extern crate markdown_parser;
use markdown_parser::ImageType;

fn get_image(s: &str) -> ImageType {
    match s.parse() {
        Ok(img) => img,
        Err(why) => panic!("{:?}", why),
    }
}

#[test]
fn absolute_image() {
    let txt = "![MacDown logo](http://macdown.uranusjr.com/static/images/logo-160.png)";
    match get_image(txt) {
        ImageType::NamedAbsolute(name, url) => {
            assert_eq!(name, "MacDown logo");
            assert_eq!(
                url,
                "http://macdown.uranusjr.com/static/images/logo-160.png"
            );
        }
        img => panic!("Expected NamedAbsolute image, found {:?}", img),
    }
}

#[test]
fn relative_image() {
    let txt = "![MacDown logo](../static/images/logo-160.png)";
    match get_image(txt) {
        ImageType::NamedRelative(name, url) => {
            assert_eq!(name, "MacDown logo");
            assert_eq!(url, "../static/images/logo-160.png");
        }
        img => panic!("Expected NamedRelative image, found {:?}", img),
    }
}
